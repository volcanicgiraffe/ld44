﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logo : MonoBehaviour
{
    float amplitude = 0.1f;
    float frequency = 1f;

    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    bool triggered = false;

    private void Start()
    {
        posOffset = transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (triggered || other.tag != "Player") return;

        triggered = true;

        Destroy(gameObject, 10f);
    }

    private void Update()
    {
        if (triggered)
        {
            Vector3 target = new Vector3(transform.position.x, 50, transform.position.z);

            transform.position = Vector3.Lerp(transform.position, target, 0.5f * Time.deltaTime);
        }
        else
        {
            tempPos = posOffset;
            tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

            transform.position = tempPos;
        }
    }
}
