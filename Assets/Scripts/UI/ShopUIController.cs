﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShopUIController : MonoBehaviour
{

    private GameSessionState gameSessionState;

    public UIUnitPanelScript prefab;
    public UIShopItemPanelScript shopItemPrefab;
    public GameObject unitsListContent;
    public GameObject shopListContent;

    public UIUnitPanelScript selected;
    public UIEquipmentScript selectedEquip;
    public AudioClip buySound;

    public Text selectedName;
    public UIHPBarScript selectedHP;
    public Text selectedHPText;
    public Text selectedRankText;
    public Text selectedKillsText;
    public Text selectedAchievementsText;
    public RawImage selectedPortrait;
    protected AudioSource audioPlayer;

    public RawImage shopBG;

    // Start is called before the first frame update
    void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
        gameSessionState = GameSessionState.GetInstance();

        bool setSelected = true;
        foreach (UnitState unitState in gameSessionState.units)
        {
            AddUnit(unitState, setSelected);
            setSelected = false;

        }

        //todo: no units left - gameover

        foreach (EquipmentScript equipment in gameSessionState.shopState.equipmentToBuy)
        {
            UIShopItemPanelScript panel = Instantiate(shopItemPrefab);
            panel.Set(equipment);
            panel.transform.SetParent(shopListContent.transform);
            panel.OnBuy += Panel_OnBuy;
        }

        foreach (UnitState unitState in gameSessionState.unitsToHire)
        {
            UIShopItemPanelScript panel = Instantiate(shopItemPrefab);
            panel.Set(unitState);
            panel.transform.SetParent(shopListContent.transform);
            panel.OnHire += Panel_OnHire;
        }

        Panel_OnSelect(selected);

    }

    void AddUnit(UnitState unitState, bool select)
    {
        UIUnitPanelScript panel = Instantiate(prefab);
        panel.OnSelect += Panel_OnSelect;
        panel.SetUnitState(unitState);
        panel.transform.SetParent(unitsListContent.transform);

        if (select) panel.SetSelected(select);
    }

    void Panel_OnHire(UnitState unit)
    {
        gameSessionState.Hire(unit, selected.unitState);
        AddUnit(unit, false);
        selected.Redraw();
        Panel_OnSelect(selected);
    }


    void Panel_OnBuy(EquipmentScript equip)
    {
        PlaySound(buySound);
        gameSessionState.Buy(equip, selected.unitState);
        selected.Redraw();
        Panel_OnSelect(selected);
    }

    public void PlaySound(AudioClip clip)
    {
        audioPlayer.clip = clip;
        if (audioPlayer != null && audioPlayer.clip != null) audioPlayer.Play();
    }


    void Panel_OnSelect(UIUnitPanelScript panel)
    {
        foreach (UIUnitPanelScript p in unitsListContent.transform.GetComponentsInChildren<UIUnitPanelScript>())
        {
            if (p != panel) p.SetSelected(false);
        }
        selected = panel;

        foreach (UIShopItemPanelScript p in shopListContent.transform.GetComponentsInChildren<UIShopItemPanelScript>())
        {
            p.UpdateEnabled(selected.unitState);
        }
        UpdateSelected();
    }

    void UpdateSelected()
    {
        selectedName.text = selected.unitState.Name.ToUpper();
        selectedHPText.text = selected.unitState.IsDead() ? "-" : selected.unitState.Health + "";
        selectedRankText.text = selected.unitState.IsDead() ? "R.I.P" : selected.unitState.Rank;
        selectedKillsText.text = $"KILLS: <color=#d7b063>{selected.unitState.Kills}</color>";
        selectedAchievementsText.text = selected.unitState.AchievementsString;
        selectedHP.HealthFromMax = selected.unitState.HealthFromMax();
        selectedPortrait.enabled = !selected.unitState.IsDead();
        selectedEquip.SetUnitState(selected.unitState);

        if (selected.unitState.IsDead())
        {
            ApplyAlpha(shopBG, 0.5f);
            ApplyAlpha(selectedEquip.GetComponent<RawImage>(), 0.5f);
        }
        else
        {
            ApplyAlpha(shopBG, 1);
            ApplyAlpha(selectedEquip.GetComponent<RawImage>(), 1);
        }
    }

    public static void ApplyAlpha(RawImage ri, float alpha)
    {
        Color c = ri.color;
        c.a = alpha;
        ri.color = c;
    }


    // Update is called once per frame
    void Update()
    {

    }


    public void OnNext()
    {
        gameSessionState.LoadNextLevel();
    }
}
