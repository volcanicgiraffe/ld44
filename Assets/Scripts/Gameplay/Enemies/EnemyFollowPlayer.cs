﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyFollowPlayer : MonoBehaviour
{
    NavMeshAgent nav;
    
    [HideInInspector]
    public GameObject victim;

    float nextTargetDelay = 1.0f;
    float timer;

    int disableRequests = 0;

    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        timer = nextTargetDelay;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!nav.enabled) return;

        if (timer > 0) timer -= Time.deltaTime;

        if (victim == null && timer < 0)
        {
            timer = nextTargetDelay;
            AssignVictim();
        }

        // todo: icxon стоит это делать пореже, а не каждый тик
        if (victim != null)
        {
            nav.SetDestination(victim.transform.position);
        }
    }
    
    public void OnTrap()
    {
        nav.isStopped = true;
    }

    public void DisableNav()
    {
        disableRequests++;
        nav.enabled = false;
    }
    public void EnableNav()
    {
        disableRequests--;
        nav.enabled = disableRequests <= 0;
        victim = null;
    }

    void AssignVictim()
    {
        victim = FindNextVictim();
    }

    GameObject FindNextVictim()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Player");

        if (gos == null || gos.Length == 0) return null;

        int i = Random.Range(0, gos.Length);

        return gos[i];
    }
}
