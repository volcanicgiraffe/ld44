﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/MonsterShader1"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Noise ("Noise", 2D) = "noise" {}
        
        [HDR]
        _NoiseColor("NoiseColor(HDR)", Color) = (20,0,0,1)
        _NoiseTresh1("Noise Treshl", Float) = 0.4
        
        _NoiseWeight("Weight", Float) = 0
        
        [PerRendererData]
        _AppearingWeight("Appear Weight", Float) = 1 //from -1 to 1. 0->1 - appearing. -1 -> 0 - disappearing
        [HDR]
        _AppearingColor("AppearColor(HDR)", Color) = (20,0,20,1)
        [HDR]
        _DisappearingColor("DisppearColor(HDR)", Color) = (4,0,0,1)
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" "Monster"="Monster"  }
        Blend SrcAlpha OneMinusSrcAlpha
        
        Pass {
            ZWrite On
            ColorMask 0
        }
        
        //GrabPass { "_MyGrabTexture" }
        LOD 200


        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf NoLight fullforwardshadows alpha// vertex:vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        //sampler2D _MyGrabTexture;
        sampler2D _Noise;
        
        half4 LightingNoLight(SurfaceOutput o, float3 lightDir, float3 viewDir, float shadow)
        {
            return half4(o.Albedo, o.Alpha);
        }

        struct Input
        {
            //float2 empty;
            float2 uv_MainTex;
            //float4 grabUV;
        };
        
        void vert (inout appdata_full v, out Input o) {
            //float4 hpos = UnityObjectToClipPos (v.vertex);
            //o.grabUV = ComputeGrabScreenPos(UnityObjectToClipPos (v.vertex));
            //o.uv_MainTex = 
        }

        fixed4 _Color;
        fixed4 _NoiseColor;
        float _NoiseTresh1;
        float _NoiseWeight;
        float _AppearingWeight;
        fixed4 _AppearingColor;
        fixed4 _DisappearingColor;
        
        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutput o)
        {
            //half4 grabbed = tex2Dproj(_MyGrabTexture, UNITY_PROJ_COORD(IN.grabUV) + float4(0.1, 0.1, 0.1, 1));// * _Color;
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            fixed4 n = tex2D (_Noise, IN.uv_MainTex*5+_Time.x/10);
            
            //o.Albedo = lerp(grabbed.rgb, float4(0,0,0,1), 0.8 + 0.4*sin(_Time.x*100));
            // Metallic and smoothness come from slider variables
            //o.Metallic = _Metallic;
            //o.Smoothness = _Glossiness;
            float treshold = _NoiseTresh1 + 0.05*sin(_Time.x*10);
            
            //appear - from 0 to 1
            //step(appear, n.r)
            
            //every pixel goes trhough: invisible -> purple -> black
            
            float absWeight = abs(_AppearingWeight);
            fixed4 absColor = lerp(_DisappearingColor, _AppearingColor, step(0, _AppearingWeight));
            
            float appearVisible = step(absWeight, n.r);
            float appearPurple = step(absWeight-0.1, n.r);
            fixed3 appearColor = lerp(_Color, absColor, appearPurple);
            float appearAlpha = 1 - appearVisible;
            
            
            float flag = 1 - step(treshold, n.r);
            o.Albedo = lerp(appearColor, _NoiseColor, (1-flag)*_NoiseWeight);
            o.Alpha = appearAlpha * (0.5 +(lerp(1, flag, _NoiseWeight))*0.5);//flag;//c.a * 0.3 * grabbed.x;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
