﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUnitPanelScript : MonoBehaviour
{

    public UnitState unitState;
    private bool selected = false;

    public delegate void SelectedAction(UIUnitPanelScript panel);

    public event SelectedAction OnSelect;


    public void SetUnitState(UnitState state)
    {
        this.unitState = state;
        this.Redraw();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSelected(bool flag)
    {
        selected = flag;
        if (selected) OnSelect?.Invoke(this);

    }

    public void OnClick()
    {
        SetSelected(true);
    }

    public void Redraw()
    {
        transform.Find("NameBg").GetComponentInChildren<Text>().text = unitState.Name.ToUpper();
        transform.Find("BloodContainer").GetComponent<UIHPBarScript>().HealthFromMax = unitState.HealthFromMax();
        if (unitState.IsDead())
        {
            transform.Find("Portrait").GetComponent<RawImage>().enabled = false;
        }
    }
}
