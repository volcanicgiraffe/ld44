﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShopItemPanelScript : MonoBehaviour
{
    private UnitState unitToHire;
    private EquipmentScript equipmentPrefab;

    public delegate void HireAction(UnitState unit);
    public delegate void BuyAction(EquipmentScript equip);

    public event HireAction OnHire;
    public event BuyAction OnBuy;

    public Texture defaultPortrait;

    public void Set(UnitState unitToHire)
    {
        this.unitToHire = unitToHire;
        Set(unitToHire.Name, unitToHire.Health + "", "Hire", defaultPortrait, unitToHire.Rank, unitToHire.Description);
    }

    public void Set(EquipmentScript equipment)
    {
        if (equipment == null) return;
        this.equipmentPrefab = equipment;
        Set(equipment.Name, equipment.HealthCost + "", "Buy", equipment.Icon, equipment.Stats, equipment.Description);
    }

    public void Set(string name, string cost, string buttonName, Texture image, string stats, string description)
    {
        transform.Find("Name").GetComponent<Text>().text = name.ToUpper();
        transform.Find("Stats").GetComponent<Text>().text = stats;
        transform.Find("Description").GetComponent<Text>().text = description;
        transform.Find("Cost").GetComponent<Text>().text = cost;
        //transform.Find("BuyOrHire").transform.GetChild(0).GetComponent<Text>().text = buttonName;
        transform.Find("Image").GetComponent<RawImage>().texture = image;
    }

    public void UpdateEnabled(UnitState selectedUnit)
    {
        if (!selectedUnit.IsDead() && selectedUnit.Health > Cost())
        {
            SetEnabled(true);
        }
        else
        {
            SetEnabled(false);
        }
    }

    public void OnClick()
    {
        if (equipmentPrefab != null)
            OnBuy?.Invoke(equipmentPrefab);
        else
            OnHire?.Invoke(unitToHire);
        Destroy(this.gameObject);
    }

    public int Cost()
    {
        return this.unitToHire?.Health ?? equipmentPrefab?.HealthCost ?? 0;
    }

    void SetEnabled(bool flag)
    {
        GetComponent<Button>().interactable = flag;
        //RawImage overlay = transform.Find("StatusOverlay").GetComponent<RawImage>();
        //ShopUIController.ApplyAlpha(overlay, flag ? 0 : 0.5f);

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
