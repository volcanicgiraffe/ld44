﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScene : MonoBehaviour
{
    public GameObject logo;

    bool done = false;

    public void WinSequence()
    {
        if (done) return;
        done = true;

        CameraMover cam = null;
        var camGo = GameObject.FindGameObjectWithTag("MainCamera");
        if (camGo != null) cam = camGo.GetComponent<CameraMover>();
        if (cam != null) cam.ShowWin();


        logo.SetActive(true);
    }
}
