﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : EquipmentScript
{

    public float fireRate = 0f;
    public Transform muzzle;
    public AudioClip[] fireSounds = new AudioClip[] { };
    public float range = 5f;
    public string DamageStr;

    public GameObject bulletPrefab;

    public bool stationary = false;

    protected float timerRate;
    protected Animator anim;
    protected AudioSource audioPlayer;


    public override string Stats
    {
        get { return $"Damage: {DamageStr}  Fire rate: {60 / fireRate}  Range: {range}"; }
    }

    void Start()
    {
        anim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();

        timerRate = fireRate;
    }

    void Update()
    {
        if (timerRate > 0)
        {
            timerRate -= Time.deltaTime;
        }
    }

    public virtual void Shoot(Transform target)
    {
        if (timerRate > 0) return;
        timerRate = fireRate;

        gameObject.transform.LookAt(target);

        GameObject bullet = Instantiate(bulletPrefab, muzzle.position, gameObject.transform.rotation);
        PlaySound();

    }

    public void PlaySound()
    {
        if (fireSounds.Length > 0)
        {
            var sound = Random.Range(0, fireSounds.Length);
            audioPlayer.clip = fireSounds[sound];
        }
        if (audioPlayer != null && audioPlayer.clip != null) audioPlayer.Play();
    }

    override public bool IsWeapon()
    {
        return true;
    }
}
