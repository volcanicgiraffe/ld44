﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentScript : MonoBehaviour
{
    public string Name;
    public string Description;

    public bool ShallRemoveFromShop = false;

    public string StringStats = "";
    public virtual string Stats
    {
        get { return StringStats; }
    }

    public int HealthCost;

    public Texture Icon;

    virtual public bool IsWeapon()
    {
        return false;
    }
}
