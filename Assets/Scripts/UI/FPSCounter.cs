﻿using System;
using UnityEngine;
using TMPro;

namespace UnityStandardAssets.Utility
{
    public class FPSCounter : MonoBehaviour
    {
        public TMP_Text m_Text;
        const float fpsMeasurePeriod = 0.5f;
        private int m_FpsAccumulator = 0;
        private float m_FpsNextPeriod = 0;
        private int m_CurrentFps;
        const string display = "{0} FPS";

        Color good = Color.green;
        Color medium = Color.yellow;
        Color bad = Color.red;

        private void Start()
        {
            m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
        }


        private void Update()
        {
            m_FpsAccumulator++;
            if (Time.realtimeSinceStartup > m_FpsNextPeriod)
            {
                m_CurrentFps = (int)(m_FpsAccumulator / fpsMeasurePeriod);
                m_FpsAccumulator = 0;
                m_FpsNextPeriod += fpsMeasurePeriod;
                m_Text.text = string.Format(display, m_CurrentFps);
                if (m_CurrentFps >= 60)
                {
                    m_Text.color = good;
                }
                else if (m_CurrentFps >= 30 && m_CurrentFps < 60)
                {
                    m_Text.color = medium;
                }
                else
                {
                    m_Text.color = bad;
                }
            }
        }
    }
}
