﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableTrap : ThrowableItem
{

    bool setUp = false;
    bool triggered = false;

    SphereCollider trigger;
    
    EnemyFollowPlayer trappedEnemy;

    protected override void Start()
    {
        base.Start();
        trigger = GetComponent<SphereCollider>();
    }

    protected override void Update()
    {
        base.Update();

        if (!inAir && !setUp)
        {
            setUp = true;
            transform.localScale = new Vector3(1f, 1f, 1f);
            trigger.radius = 0.7f; // радиус триггера после установки
        }

        // Значит врага убили пока он был в ловушке
        if (triggered && trappedEnemy == null)
        {
            Destroy(gameObject); // todo: красота
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (inAir)
        {
            inAir = false;
            rig.velocity = Vector3.zero;
            rig.angularVelocity = Vector3.zero;
            transform.rotation = Quaternion.identity;
        }

        trappedEnemy = other.GetComponent<EnemyFollowPlayer>();

        if (setUp && !triggered && trappedEnemy != null)
        {
            triggered = true;
            transform.rotation = Quaternion.Euler(90, 0, 0); // TODO: triggred model or animation
            audioPlayer.clip = Action;
            if (audioPlayer != null && audioPlayer.clip != null) audioPlayer.Play();
            trappedEnemy.OnTrap();
        }

    }
}
