﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Полиморфик для гранаты и капкана, у них разные реализации триггера
public class ThrowableItem : EquipmentScript
{
    // TODO: лететь к цели по дуге или ещё как :D

    int force = 600;

    protected Rigidbody rig;
    protected bool inAir = true;
    public AudioClip Throw;
    public AudioClip Action;

    Vector3 randRotation;
    protected AudioSource audioPlayer;

    protected virtual void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
        audioPlayer.clip = Throw;
        if (audioPlayer != null && audioPlayer.clip != null) audioPlayer.Play();
        rig = GetComponent<Rigidbody>();
        rig.AddForce(transform.forward * force);

        randRotation = new Vector3(Random.Range(-180.0f, 180.0f), Random.Range(-180.0f, 180.0f), Random.Range(-180.0f, 180.0f));
    }

    protected virtual void Update()
    {
        if (inAir) transform.Rotate(randRotation.x * Time.deltaTime, randRotation.y * Time.deltaTime, randRotation.z * Time.deltaTime);
    }
}
