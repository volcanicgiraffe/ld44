﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMelee : Weapon
{

    public override void Shoot(Transform target)
    {
        if (timerRate > 0) return;
        timerRate = fireRate;

        if (anim != null) anim.SetTrigger("Swing");
        PlaySound();
    }
}
