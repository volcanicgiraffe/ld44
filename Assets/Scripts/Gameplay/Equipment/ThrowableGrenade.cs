﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableGrenade : ThrowableItem
{
    public float explosionRadius;
    public float explosionForce;
    public int explosionDamage;
    public float stunTime;

    public GameObject explosionPrefab;


    bool exploded = false;

    private void OnTriggerEnter(Collider other)
    {
        inAir = false;

        if (exploded) return;
        exploded = true;
        audioPlayer.clip = Action;
        if (audioPlayer != null && audioPlayer.clip != null) audioPlayer.Play();
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, LayerMask.GetMask("Enemy"));
        
        foreach (Collider hit in colliders)
        {
            SimpleEnemy enemy = hit.GetComponent<SimpleEnemy>();
            if (enemy == null) continue;

            enemy.StunBy(this);
        }
        // wont work without outside sound manager
        if (explosionPrefab != null) Instantiate(explosionPrefab, transform.position, transform.rotation);

        Destroy(gameObject);
    }

}
