﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : MonoBehaviour
{
    public int initialHealth = 1;
    [Range(0.0f, 1.0f)]
    public float dropChance = 0f;
    public int DropAmount = 1;

    public Transform aimPoint;
    public GameObject itemToDrop;

    public delegate void DieAction(SimpleEnemy self);
    public event DieAction OnDie;

    public int attackDamage = 1;
    public float attackCooldown = 2f;

    public GameObject deathObject;
    public ParticleSystem hitParticles;

    float attackTimer;
    int victimsInRange;
    int health;

    Rigidbody rig;
    Animator animator;
    public AudioClip spawnSound;
    public AudioClip attackSound;

    EnemyFollowPlayer mover;

    float stunTimer = -1;
    protected AudioSource audioPlayer;

    public float roarTimer = 2;


    public float appearSpeed = 0.4f;
    public float appear = 0;

    SkinnedMeshRenderer bodyRenderer;
    MaterialPropertyBlock block;

    void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
        rig = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        mover = GetComponent<EnemyFollowPlayer>();

        health = initialHealth;
        attackTimer = attackCooldown;

        bodyRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        block = new MaterialPropertyBlock();
    }

    public void PlaySound(AudioClip clip)
    {
        if (audioPlayer != null && audioPlayer.clip != null)
        {
            audioPlayer.clip = clip;
            audioPlayer.Play();
        }
    }

    void Update()
    {
        if (IsDead())
        {
            if (appear < 0)
            {
                appear += appearSpeed * Time.deltaTime;
                block.SetFloat("_AppearingWeight", Mathf.Min(0, appear));
                bodyRenderer.SetPropertyBlock(block);
            }
            else
            {
                Destroy(gameObject);
            }
            return;
        }
        if (appear < 1)
        {
            appear += appearSpeed * Time.deltaTime;
            block.SetFloat("_AppearingWeight", Mathf.Min(1, appear));
            bodyRenderer.SetPropertyBlock(block);
        }
        //if (Input.GetKeyUp(KeyCode.Space)) Die();
        if (IsDead()) return;
        roarTimer -= Time.deltaTime;
        if (roarTimer < 0)
        {
            roarTimer = Random.Range(10, 20);
            PlaySound(spawnSound);
        }
        if (attackTimer > 0) attackTimer -= Time.deltaTime;

        if (stunTimer > 0)
        {
            stunTimer -= Time.deltaTime;
            if (stunTimer <= 0)
            {
                RestoreStun();
            }
        }

        if (transform.position.y < -20) Die();
    }

    void OnTriggerEnter(Collider other)
    {
        victimsInRange++;
    }

    void OnTriggerExit(Collider other)
    {
        victimsInRange--;
    }

    void OnTriggerStay(Collider other)
    {
        if (IsDead()) return;
        if (other.tag != "Player") return;

        if (victimsInRange > 0 && attackTimer < 0)
        {
            var unit = other.gameObject.GetComponent<ControllableUnitScript>();
            if (unit == null)
            {
                Debug.LogWarning("## Player without ControllableUnitScript! " + other.gameObject);
                return;
            }

            //melee
            unit.Hit(attackDamage);
            PlaySound(attackSound);
            if (animator != null) animator.SetTrigger("Attack");

            attackTimer = attackCooldown;
        }
    }

    public Transform AimPoint()
    {
        if (aimPoint != null) return aimPoint;
        else return transform;
    }

    public virtual void OnHit(SimpleDamage by)
    {
        onHit(by.damage, by.transform.position);
    }

    void onHit(int dmg, Vector3 hitPoint)
    {
        if (IsDead()) return;

        health -= dmg;

        if (health <= 0)
        {
            Die();
        }

        if (hitParticles != null)
        {
            hitParticles.transform.position = hitPoint;
            hitParticles.Play();
        }
    }

    public virtual void StunBy(ThrowableGrenade grenade)
    {
        onHit(grenade.explosionDamage, AimPoint().position);

        if (rig == null || IsDead()) return;
        if (stunTimer <= 0) mover.DisableNav();
        rig.isKinematic = false;
        rig.AddExplosionForce(grenade.explosionForce, transform.position - Vector3.up, grenade.explosionRadius);
        stunTimer = grenade.stunTime;
    }

    void RestoreStun()
    {
        if (rig == null || IsDead()) return;

        rig.isKinematic = true;
        mover.EnableNav();
    }

    public bool IsDead()
    {
        return health <= 0;
    }

    public virtual void Die()
    {
        health = -1;
        appear = -0.7f;
        appearSpeed *= 2;
        if (deathObject != null)
        {
            Instantiate(deathObject, transform.position, transform.rotation);
        }

        if (itemToDrop != null && Random.value < dropChance)
        {

            for (int i = 0; i < DropAmount; i++)
            {
                Vector3 pos = transform.position + new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1));
                Instantiate(itemToDrop, pos, transform.rotation);
            }

        }

        //Destroy(gameObject, 3);
        OnDie?.Invoke(this);
    }
}
