﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEquipmentScript : MonoBehaviour
{
    public bool IsInGame = false;

    private UnitState unitState;

    public void SetUnitState(UnitState state)
    {
        this.unitState = state;
        this.Redraw();
    }

    public void Redraw()
    {
        if (unitState.ActiveWeapon != null)
        {
            transform.Find("WeaponImage").GetComponent<RawImage>().texture = unitState.ActiveWeapon.Icon;
            transform.Find("WeaponName").GetComponent<Text>().text = unitState.ActiveWeapon.Name.ToUpper();
        }
        transform.Find("WeaponImage").GetComponent<RawImage>().enabled = unitState.ActiveWeapon != null;
        transform.Find("WeaponName").GetComponent<Text>().enabled = unitState.ActiveWeapon != null;

        RedrawEquip(transform.Find("Eq1").GetComponent<RawImage>(), unitState.Equipment, 0);
        RedrawEquip(transform.Find("Eq2").GetComponent<RawImage>(), unitState.Equipment, 1);
        RedrawEquip(transform.Find("Eq3").GetComponent<RawImage>(), unitState.Equipment, 2);
    }

    private void RedrawEquip(RawImage image, List<EquipmentScript> eqs, int idx)
    {
        EquipmentScript eq = idx >= eqs.Count ? null : eqs[idx];
        image.enabled = eq != null;
        if (eq != null) image.texture = eq.Icon;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
