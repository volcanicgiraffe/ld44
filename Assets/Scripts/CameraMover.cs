﻿using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public int Speed = 10;
    public Transform winPosition;

    float distance;

    float dx;
    float dz;

    Transform target;
    bool win = false;
    Vector3 velocity = Vector3.zero;

    private void Start()
    {
        dx = transform.position.x;
        dz = transform.position.z;

        // хочу двигать камеру относитьно юнита на том же расстоянии на котором камера от земли сейчас
        // т.к не умею работать с векторами, считаю ручками.
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit))
        {
            distance = hit.distance;
            dx = transform.position.x - hit.point.x;
            dz = transform.position.z - hit.point.z;   
        }
    }
    public void ShowWin()
    {
        win = true;
    }

    void Update()
    {
        float speedCorrected = Speed / Time.timeScale;

        if (win)
        {
            transform.position = Vector3.Lerp(transform.position, winPosition.position, speedCorrected * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, winPosition.rotation, speedCorrected * Time.deltaTime);

            return;
        }

        if (target == null) return;

        Vector3 destination = new Vector3(target.position.x + dx, transform.position.y, target.position.z + dz);

        float dist = Vector3.Distance(transform.position, destination);

        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, Speed * Time.deltaTime, Speed * dist);
        /*
        
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(new Vector3(speedCorrected * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(new Vector3(-speedCorrected * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(new Vector3(0, -speedCorrected * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(new Vector3(0, speedCorrected * Time.deltaTime, 0));
        }*/


    }

    public void SetTarget(Transform t)
    {
        target = t;
    }

      
}
