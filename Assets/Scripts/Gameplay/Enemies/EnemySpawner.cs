﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class SpawnWave
{
    public float Delay = 0;
    public float Rate = 0;
    public float NextMobIn = 0;
    public GameObject Mob;
    public int Amount = 0;
}



public class EnemySpawner : MonoBehaviour
{
    public Color GizmosColor = new Color(1f, 0.1f, 0.1f, 0.1f);
    public SpawnWave[] Waves = new SpawnWave[] { };

    bool ended = false;

    public delegate void EnemySpawnAction(GameObject enemy, int remaining);

    public event EnemySpawnAction OnEnemySpawn;


    void Start()
    {
    }

    int getSpawns()
    {
        int result = 0;
        foreach (SpawnWave w in Waves)
        {
            result += w.Amount;
        }
        return result;
    }

    void Update()
    {
        if(ended) return;
        int spawnsLeft = getSpawns();
        if (spawnsLeft == 0)
        {
            ended = true;
            return;
        }
        for (int i = 0; i < Waves.Length; i++)
        {
            SpawnWave w = Waves[i];
            if (w.Amount <= 0)
            {
                continue;
            }
            if (w.Delay > 0)
            {
                w.Delay -= Time.deltaTime;
                continue;
            }

            if (w.NextMobIn > 0)
            {
                w.NextMobIn -= Time.deltaTime;
                continue;
            }
            w.NextMobIn = w.Rate;
            w.Amount--;
            spawnsLeft--;
            SpawnOnce(w.Mob, spawnsLeft);
        }

    }

    void SpawnOnce(GameObject enemyPrefab, int spawnsLeft)
    {
        if (enemyPrefab == null)
        {
            Debug.LogWarning("Spawner is empty: " + transform.position);
            return;
        }

        GameObject go = Instantiate(enemyPrefab, transform.position, transform.rotation);
        OnEnemySpawn?.Invoke(go, spawnsLeft);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = GizmosColor;
        Gizmos.DrawCube(transform.position, transform.localScale);
    }
}
