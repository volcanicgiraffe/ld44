﻿using UnityEngine;
using System.Collections;

public class ShopAdder : MonoBehaviour
{

    public EquipmentScript[] AddToShopItems;

    public UnitState[] AddUnitsToHire;

    private bool subscribed = false;
    private void Update()
    {
        if (subscribed) return;
        LevelStateScript ls = Object.FindObjectOfType<LevelStateScript>();
        if (ls == null) return;
        ls.OnLevelComplete += Handle_OnLevelComplete;

        subscribed = true;
    }

    void Handle_OnLevelComplete()
    {
        GameSessionState state = GameSessionState.GetInstance();

        state.Add(AddToShopItems, AddUnitsToHire);
    }

}
