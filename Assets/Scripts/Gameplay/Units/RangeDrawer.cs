﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeDrawer : MonoBehaviour
{
    public LineRenderer line;
    public float arc = 360f;

    int segments = 100;
    float range = 10f;


    void Start()
    {
        line.positionCount = segments + 1;
        line.useWorldSpace = false;
        CreatePoints();
    }

    void CreatePoints()
    {
        float x;
        float z;

        float angle = 20f;

        Vector3[] positions = new Vector3[segments + 1];

        for (int i = 0; i < (segments + 1); i++)
        {
            float unitScale = 0.33f; // todo: Юнит отскейлен, эт плохо.

            x = Mathf.Sin(Mathf.Deg2Rad * angle) * range / unitScale;
            z = Mathf.Cos(Mathf.Deg2Rad * angle) * range / unitScale;

            positions[i] = new Vector3(x, 0, z);

            // line.SetPosition(i, new Vector3(x, 0, z));

            angle += (arc / segments);
        }

        line.SetPositions(positions);
    }

    public void Show(float r, float a)
    {
        if (a != arc && a > 0)
        {
            arc = a;
            CreatePoints();
        }
        if (r != range && r > 0)
        {
            range = r;
            CreatePoints();
        }

        line.enabled = true;
    }

    public void Hide()
    {
        line.enabled = false;
    }
}
