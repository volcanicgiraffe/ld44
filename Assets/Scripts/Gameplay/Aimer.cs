﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aimer : MonoBehaviour
{
    private bool visible = false;

    // Start is called before the first frame update
    void Start()
    {
        Hide();
    }


    // Update is called once per frame
    void Update()
    {
        if (!visible) return;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "WalkableTerrain")
            {
                transform.position = hit.point;
            }
        }
    }

    public void Show()
    {
        visible = true;
    }

    public void Hide()
    {
        transform.position = new Vector3(0, -10, 0);
        visible = false;
    }
}
