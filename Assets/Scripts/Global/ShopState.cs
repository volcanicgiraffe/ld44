﻿using System;
using System.Collections.Generic;
using UnityEngine;

// состояние магазина
public class ShopState
{
    public List<EquipmentScript> equipmentToBuy = new List<EquipmentScript>();


    public void Add(EquipmentScript prefab)
    {
        equipmentToBuy.Add(prefab);
    }

    public EquipmentScript Buy(EquipmentScript obj)
    {
        if (!equipmentToBuy.Contains(obj)) return null;
        if (obj.ShallRemoveFromShop)
        {
            equipmentToBuy.Remove(obj);
        }

        return obj;//no buy
    }

}

