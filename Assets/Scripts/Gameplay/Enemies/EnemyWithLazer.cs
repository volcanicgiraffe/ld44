﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class EnemyWithLazer : MonoBehaviour
{

    private ControllableUnitScript target;
    private bool rotateTo = false;
    public GameObject LazerBeamPrefab;
    public GameObject eye;
    public float lazerLifetime = 0.2f;
    public float vision = 4f;

    EnemyFollowPlayer mover;
    SimpleEnemy enemy;

    float attackTimer;


    private void Start()
    {
        mover = GetComponent<EnemyFollowPlayer>();
        enemy = GetComponent<SimpleEnemy>();
        attackTimer = enemy.attackCooldown;
    }

    public void DoFire(ControllableUnitScript target)
    {
        GetComponent<Animator>().Play("Attack");
        enemy.PlaySound(enemy.attackSound);
        this.target = target;
        mover.DisableNav();
    }


    public void OnStartFireEvent()
    {
        rotateTo = true;
    }


    public void OnFireEvent()
    {
        if (target == null || target.headSlot == null) return;

        // Debug.Log("I'm called");
        rotateTo = false;
        GameObject lazerBeam = GameObject.Instantiate(LazerBeamPrefab, transform);
        //do some math
        //i have point 1 - my position
        //i have point 2 - enemy
        //i need to put lazer beam in middle point and scale accordingly
        Vector3 p1 = eye.transform.position;
        Vector3 p2 = target.headSlot.position;

        Vector3 middle = (p1 + p2) / 2;
        lazerBeam.transform.position = middle;
        lazerBeam.transform.LookAt(p2);
        lazerBeam.transform.localScale = new Vector3(1, 1, (p2 - p1).magnitude / 2);

        Object.Destroy(lazerBeam, lazerLifetime);

        target.Hit(GetComponent<SimpleEnemy>().attackDamage);

    }

    private void Update()
    {
        if (enemy.IsDead()) return;

        CheckAttack();

        if (rotateTo)
        {
            if (target == null || target.headSlot == null) return;
            //todo: я сделал спецом резкий поворот, чтобы лазер нормально пулял прямо, а не под углом
            Vector3 lookTo = target.headSlot.position;
            lookTo.y = transform.position.y;
            transform.LookAt(lookTo);
        }
    }

    void CheckAttack()
    {
        if (attackTimer > 0) attackTimer -= Time.deltaTime;

        if (attackTimer < 0)
        {
            attackTimer = enemy.attackCooldown;

            Collider[] colliders = Physics.OverlapSphere(transform.position, vision, LayerMask.GetMask("Player"));

            foreach (Collider hit in colliders)
            {
                ControllableUnitScript unit = hit.GetComponent<ControllableUnitScript>();
                if (unit != null)
                {
                    DoFire(unit);
                    return;
                }
            }

        }
    }

    public void OnFireEnd()
    {
        mover.EnableNav();
        target = null;
    }
}
