﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBulletDamage : SimpleDamage
{
    float aliveTime = 15f;

    private void Start()
    {
        Destroy(gameObject, aliveTime); // todo: плавное исчезновение
    }

    private void OnTriggerEnter(Collider other)
    {
        SimpleEnemy enemy = other.gameObject.GetComponent<SimpleEnemy>();

        if (enemy != null)
        {
            enemy.OnHit(this);
        }
    }
}
