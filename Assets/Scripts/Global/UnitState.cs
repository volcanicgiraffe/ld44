﻿using System;
using System.Collections.Generic;
using UnityEngine;




/*
 * тут хранится инфа о бойце, передаваемая между уровнями
 */
[System.Serializable]
public class UnitState
{
    public string Name;
    public string Description = "Damned soul in a search of salvation";
    public int Health;
    public int MaxHealth = 100;
    public int Kills = 0;

    public Weapon ActiveWeapon = null;
    public List<EquipmentScript> Equipment = new List<EquipmentScript>(3);

    public List<string> Achievements = new List<string>();
    public string AchievementsString
    {
        get { return String.Join("\n", Achievements.ToArray()); }
    }
    public string Rank
    {
        get
        {
            if (Kills > 130) return "AESCULAPIUS";
            if (Kills > 100) return "SENIOR EXORCIST";
            if (Kills > 75) return "BLOOD MASTER";
            if (Kills > 50) return "SURGEON";
            if (Kills > 25) return "ADEPT OF PAIN";
            if (Kills > 10) return "APPRENTICE DOCTOR";
            return "NOVICE";
        }
    }

    public UnitState()
    {

    }

    public float HealthFromMax()
    {
        return (float)Health / (float)MaxHealth;
    }


    public bool IsDead()
    {
        return Health <= 0;
    }

    public void EarnedHP(int count)
    {
        Health += count;
        Health = Math.Min(Health, MaxHealth);
    }

    internal void AddEquipment(EquipmentScript equipmentScript)
    {
        if (equipmentScript.IsWeapon())
        {
            ActiveWeapon = equipmentScript as Weapon;
        }
        else
        {
            for (int i = 0; i < Equipment.Count; i++)
            {
                if (Equipment[i] == null)
                {
                    Equipment[i] = equipmentScript;
                    return;
                }
            }

            Equipment.Add(equipmentScript);
            if (Equipment.Count > 3)
            {
                Equipment.RemoveAt(0);
            }
        }
    }
}

