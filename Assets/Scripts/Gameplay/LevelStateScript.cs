﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelStateScript : MonoBehaviour
{
    public List<ControllableUnitScript> controllableUnits = new List<ControllableUnitScript>();
    public ControllableUnitScript selectedUnit;

    public Text selectedUnitText;
    private SpawnArea unitsSpawnArea;
    public GameObject unitPrefab;
    public AudioClip unitDeathSound;
    public AudioClip[] enemyDeathSounds = new AudioClip[] { };
    protected AudioSource audioPlayer;
    public InGameUIScript inGameUI;

    private GameSessionState gameSessionState;

    private int remainingSpawners = 0;
    private int aliveEnemies = 0;
    private int aliveHeroes = 0;

    public delegate void CompletionAction();

    public event CompletionAction OnLevelComplete;

    WinScene winScene;

    // Start is called before the first frame update
    void Start()
    {
        GameObject winGo = GameObject.FindGameObjectWithTag("WinScene");
        if (winGo != null) winScene = winGo.GetComponent<WinScene>();


        unitsSpawnArea = GameObject.FindGameObjectWithTag("UnitSpawnArea").GetComponent<SpawnArea>();
        inGameUI = GameObject.FindObjectOfType<InGameUIScript>();
        gameSessionState = GameSessionState.GetInstance();
        audioPlayer = GetComponent<AudioSource>();

        foreach (UnitState unit in gameSessionState.units)
        {
            GameObject unitGO = unitsSpawnArea.Spawn(unitPrefab);
            ControllableUnitScript unitScript = unitGO.GetComponent<ControllableUnitScript>();
            unitScript.unitState = unit;
            controllableUnits.Add(unitScript);
            aliveHeroes++;
        }

        foreach (ControllableUnitScript unit in controllableUnits)
        {
            unit.OnSelect += OnUnitSelect;
            unit.OnDeselect += OnUnitDeselect;
            unit.OnDie += OnUnitDied;
        }

        foreach (EnemySpawner enemySpawner in GameObject.FindObjectsOfType<EnemySpawner>())
        {
            enemySpawner.OnEnemySpawn += OnEnemySpawn;
            remainingSpawners++;
        }
        foreach (SimpleEnemy enemy in GameObject.FindObjectsOfType<SimpleEnemy>())
        {
            aliveEnemies++;
            enemy.OnDie += OnEnemyDied;
        }
        UpdateSelectedText();
        CheckWinConditions();
    }

    void UpdateSelectedText()
    {
        inGameUI.UpdateSelected(selectedUnit?.unitState ?? null);
    }

    // Update is called once per frame
    void Update()
    {
        int manSelect = 0;
        if (Input.GetButtonDown("Select1"))
            manSelect = 1;
        if (Input.GetButtonDown("Select2"))
            manSelect = 2;
        if (Input.GetButtonDown("Select3"))
            manSelect = 3;
        if (Input.GetButtonDown("Select4"))
            manSelect = 4;
        if (Input.GetButtonDown("Select5"))
            manSelect = 5;
        if (manSelect > 0)
        {
            var objs = GameObject.FindGameObjectsWithTag("Player");
            if (objs.Length >= manSelect && objs[manSelect - 1] != null)
            {
                var comp = objs[manSelect - 1].GetComponent<ControllableUnitScript>();
                if (selectedUnit != null)
                    selectedUnit.SetSelected(false);
                comp.SetSelected(true);
            }
        }
    }

    public void OnHitClick()
    {
        selectedUnit.Hit(15);
        UpdateSelectedText();
    }


    public void OnLooseClick()
    {
        gameSessionState.RestartGame();
    }

    public void OnWinClick()
    {
        OnLevelComplete?.Invoke();
        gameSessionState.OnLevelComplete();
    }


    public void OnEnemySpawn(GameObject enemy, int remaining)
    {
        SimpleEnemy simpleEnemy = enemy.GetComponent<SimpleEnemy>();
        if (simpleEnemy != null)
        {
            aliveEnemies++;
            simpleEnemy.OnDie += OnEnemyDied;
        }
        if (remaining == 0)
        {
            remainingSpawners--;
            CheckWinConditions();
        }
    }

    public void OnEnemyDied(SimpleEnemy diedEnemy)
    {
        PlayRandomSound(enemyDeathSounds);
        aliveEnemies--;
        CheckWinConditions();
    }

    public void OnUnitDied(ControllableUnitScript unit)
    {
        PlaySound(unitDeathSound);
        controllableUnits.Remove(unit);
        aliveHeroes--;
        CheckWinConditions();
        if (selectedUnit == unit)
        {
            selectedUnit = null;
            UpdateSelectedText();
            Time.timeScale = 1;
        }
    }

    public void PlayRandomSound(AudioClip[] list)
    {
        if (list.Length > 0)
        {
            var sound = Random.Range(0, list.Length);
            audioPlayer.clip = list[sound];
        }
        if (audioPlayer != null && audioPlayer.clip != null) audioPlayer.Play();
    }

    public void PlaySound(AudioClip clip)
    {
        if (audioPlayer != null && audioPlayer.clip != null)
        {
            audioPlayer.clip = clip;
            audioPlayer.Play();
        }
    }

    public void CheckWinConditions()
    {
        if (aliveHeroes == 0)
        {
            inGameUI.ShowFailed();
        }
        if (remainingSpawners == 0 && aliveEnemies == 0)
        {
            if (winScene != null)
            {
                winScene.WinSequence();
            }
            else
            {
                inGameUI.ShowCompleted();
            }
        }
    }

    void UpdateTime()
    {
        Time.timeScale += (1.0f / 1000f) * Time.unscaledDeltaTime; // notice we are using the unscaledDelta now
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0, 1);
    }


    public void OnUnitDeselect(ControllableUnitScript unit)
    {
        if (selectedUnit != unit) return;
        selectedUnit = null;
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
        UpdateSelectedText();
    }

    public void OnUnitSelect(ControllableUnitScript unit)
    {
        if (selectedUnit == unit) return;

        selectedUnit?.SetSelected(false, false);
        selectedUnit = unit;

        Time.timeScale = 0.2f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;

        UpdateSelectedText();
    }
}
