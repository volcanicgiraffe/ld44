﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeDamage : SimpleDamage
{

    private void OnTriggerEnter(Collider other)
    {
        SimpleEnemy enemy = other.gameObject.GetComponent<SimpleEnemy>();

        if (enemy != null)
        {
            enemy.OnHit(this);
        }
    }
}
