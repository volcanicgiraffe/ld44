﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// решил не пихать это в ControllableUnitScript, но можно смёржить, если потребуется
public class UnitAttackEnemy : MonoBehaviour
{
    ControllableUnitScript unit;
    UnitState state;

    float seekTargetDelay = 1f;
    float seekTargetTimer = 0;

    SimpleEnemy enemy;
    Weapon weapon;

    public Transform body; // покачто используется для поворота тулова ГГ к врагу при стрельбе.

    float turnSpeed = 10; // это скорость поворота к врагу и обратно к углу навмеша (направление движения может не совпадать с тем куда стреляем)

    void Start()
    {
        unit = GetComponent<ControllableUnitScript>();
        state = unit.unitState;
        
        if (state.ActiveWeapon != null)
        {
            GameObject newWeapon = Instantiate(state.ActiveWeapon.gameObject, unit.weaponSlot.position, unit.weaponSlot.rotation);
            newWeapon.transform.SetParent(unit.weaponSlot);

            weapon = newWeapon.GetComponent<Weapon>();

        }

    }

    void Update()
    {
        if (weapon == null) return; // todo: рукопашка

        if (seekTargetTimer < 0)
        {
            seekTargetTimer = seekTargetDelay;
            enemy = FindClosestEnemy(weapon.range);
        }

        seekTargetTimer -= Time.deltaTime;

        ShootAttempt();
    }


    void ShootAttempt()
    {
        if (enemy == null || (unit.IsMoving() && weapon.stationary)) {
            RotateBackToMovement();
        } else
        {
            RotateToEnemy();
            weapon.Shoot(enemy.AimPoint());
        }
    }

    void RotateToEnemy()
    {
        if (body == null) return;

        var targetRotation = Quaternion.LookRotation(enemy.transform.position - transform.position);

        targetRotation.x = body.transform.rotation.x;
        targetRotation.z = body.transform.rotation.z;

        body.transform.rotation = Quaternion.Lerp(body.transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
    } 

    void RotateBackToMovement()
    {
        if (body == null) return;

        var targetRotation = Quaternion.identity;
        body.transform.localRotation = Quaternion.Lerp(body.transform.localRotation, targetRotation, turnSpeed * Time.deltaTime);
    }

    SimpleEnemy FindClosestEnemy(float range)
    {
        Vector3 position = transform.position;

        var enemies = GameObject.FindGameObjectsWithTag("Enemy");

        float min = float.PositiveInfinity;

        SimpleEnemy closest = null;

        foreach (GameObject e in enemies)
        {
            float dist = Vector3.Distance(e.transform.position, position); // магнитуда быстрее, но у нас не так много врагов

            if (dist < min && dist <= range + 1)
            {
                SimpleEnemy candidate = e.GetComponent<SimpleEnemy>();
                bool veryClose = dist < 1f; // иногда не стреляет притык, т.к. старт луча уже в монстре

                if (candidate != null && (veryClose || EnemyInSight(candidate.AimPoint(), range)))
                {
                    closest = candidate;
                    min = dist;
                }
            }
        }
        return closest;
    }

    bool EnemyInSight(Transform target, float range)
    {

        Vector3 hostPos = transform.position;
        Vector3 targetPos = target.position;
        Ray ray = new Ray(hostPos, (targetPos - hostPos).normalized * range);
        RaycastHit hit;

        int mask = LayerMask.GetMask("Enemy", "Default");

        if (Physics.Raycast(ray, out hit, range, mask))
        {
            // мы целимся в точку внутри моба, а рейкаст воткнётся в тушу моба, не дойдя до этой точки
            if (hit.collider.gameObject.GetComponent<SimpleEnemy>() != null)
            {
                return true;
            }
        }
        return false;
    }
}
