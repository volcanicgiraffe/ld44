﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/*
 * инфа по игровой сессии (от старта до финиша)
 * заполняется на старте, меняется в процессе прохождения.
 * в теории можно схоронять
 */
public class GameSessionState
{
    public List<UnitState> units = new List<UnitState>();
    public ShopState shopState = new ShopState();
    public List<UnitState> unitsToHire = new List<UnitState>();



    public List<String> totalLevels = new List<String>();
    public List<string> levelsLeft = new List<string>();

    public GameSessionState()
    {
        units.Add(new UnitState
        {
            Name = "Tolik Bombolik",
            Health = 99,
            ActiveWeapon = Resources.Load<Weapon>("Prefabs/Gameplay/Equipment/Cannon"),
            Equipment = new List<EquipmentScript>
            {
                Resources.Load<GameObject>("Prefabs/Gameplay/Equipment/Grenade").GetComponent<EquipmentScript>()
            }
        });
        units.Add(new UnitState
        {
            Name = "Leha Neumeha",
            Health = 50
        });
        units.Add(new UnitState
        {
            Name = "Rooslan Rooslan",
            Health = 66
        });

        shopState.Add(Resources.Load<Weapon>("Prefabs/Gameplay/Equipment/Crossbow"));
        shopState.Add(Resources.Load<Weapon>("Prefabs/Gameplay/Equipment/Repeater"));
        shopState.Add(Resources.Load<Weapon>("Prefabs/Gameplay/Equipment/Cannon"));
        shopState.Add(Resources.Load<Weapon>("Prefabs/Gameplay/Equipment/Incinerator"));
        shopState.Add(Resources.Load<EquipmentScript>("Prefabs/Gameplay/Equipment/Trap"));
        shopState.Add(Resources.Load<EquipmentScript>("Prefabs/Gameplay/Equipment/Grenade"));
    }

    internal void SetLevels(string[] v)
    {
        totalLevels = new List<string>(v);
        levelsLeft = new List<string>(v);
    }

    internal void Set(UnitState[] initialUnits, EquipmentScript[] initialShopItems, UnitState[] initialHireList)
    {
        this.units = new List<UnitState>(initialUnits);
        this.unitsToHire = new List<UnitState>(initialHireList);
        this.shopState.equipmentToBuy = new List<EquipmentScript>(initialShopItems);
    }

    internal void Add(EquipmentScript[] addToShopItems, UnitState[] addUnitsToHire)
    {
        this.shopState.equipmentToBuy.AddRange(addToShopItems);
        this.unitsToHire.AddRange(addUnitsToHire);
    }

    public void LoadNextLevel()
    {
        BeforeNextLevel();
        if (levelsLeft.Count == 0)
        {
            SceneManager.LoadScene("Scenes/Gameplay/EndGame");
            //todo: win screen
        }
        else
        {
            string scene = levelsLeft[0];
            levelsLeft.RemoveAt(0);
            SceneManager.LoadScene(scene);
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Scenes/Gameplay/StartGameScene");
    }

    public void OnLevelComplete()
    {
        if (levelsLeft.Count == 0)
        {
            //todo: win screen
        }
        SceneManager.LoadScene("Scenes/UIOnly/InterlevelShopScene");
    }


    public void BeforeNextLevel()
    {
        RemoveDeadUnits();

    }

    internal void RemoveDeadUnits()
    {
        units = units.FindAll((obj) => !obj.IsDead());
    }

    private static GameSessionState instance;

    internal void Hire(UnitState unit, UnitState donor)
    {
        donor.Health -= unit.Health;
        units.Add(unit);
        unitsToHire.Remove(unit);
    }

    internal void Buy(EquipmentScript equip, UnitState donor)
    {
        donor.Health -= equip.HealthCost;
        donor.AddEquipment(shopState.Buy(equip));
       
    }

    public static GameSessionState GetInstance()
    {
        if (instance == null) instance = new GameSessionState();

        return instance;
    }

    //new game
    public static GameSessionState ResetState()
    {
        instance = null;
        return GetInstance();
    }
}

