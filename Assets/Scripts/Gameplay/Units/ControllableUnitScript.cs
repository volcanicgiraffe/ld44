﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControllableUnitScript : MonoBehaviour
{
    public GameObject[] headVariants;
    public Transform headSlot;
    public Transform weaponSlot;
    public RangeDrawer rangeDrawer;
    public RangeDrawer healthBar;
    public AudioClip[] stepSounds = new AudioClip[] { };
    public AudioClip[] hurtSounds = new AudioClip[] { };
    public AudioClip hpSound;

    public UnitState unitState;

    private Animator anim;
    private NavMeshAgent agent;
    private Collider selectionCollider;
    private Collider myCollider;
    private MeshRenderer selectionRenderer;


    public delegate void SelectedAction(ControllableUnitScript unit);
    public event SelectedAction OnSelect;
    public event SelectedAction OnDeselect;
    public event SelectedAction OnDie;

    private Aimer aimer;
    protected AudioSource audioPlayer;
    private bool selected;
    private int throwableSelected = -1; // из какого слота выбран предмет, Q = 0, W = 1, E = 2

    private float baseSpeed;
    public float stepTimer = 0.5f;
    CameraMover cam;

    // Start is called before the first frame update
    void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        baseSpeed = agent.speed;
        GameObject selection = transform.Find("Selection").gameObject;
        selectionCollider = selection.GetComponent<Collider>();
        selectionRenderer = selection.GetComponent<MeshRenderer>();
        myCollider = GetComponent<Collider>();

        GameObject newHead = Instantiate(headVariants[Random.Range(0, headVariants.Length)], headSlot.position, headSlot.rotation);
        newHead.transform.SetParent(headSlot);

        var aimerGo = GameObject.FindGameObjectWithTag("Aimer"); // должен присутствовать на сцене.
        if (aimerGo != null) aimer = aimerGo.GetComponent<Aimer>();

        var camGo = GameObject.FindGameObjectWithTag("MainCamera");
        if (camGo != null) cam = camGo.GetComponent<CameraMover>();

        SetSelected(false);

        healthBar.Show(1f, 360f * unitState.Health / 100f);
    }

    public void SetSelected(bool flag, bool fireEvent = true)
    {
        selected = flag;

        selectionRenderer.enabled = flag;
        if (flag)
        {
            if (cam != null) cam.SetTarget(transform);

            if (unitState.ActiveWeapon != null) rangeDrawer.Show(unitState.ActiveWeapon.range, 360f);
        }
        else
        {
            rangeDrawer.Hide();
        }
        if (!fireEvent) return;
        if (flag) OnSelect?.Invoke(this); else OnDeselect?.Invoke(this);
    }

    public void PlayRandomSound(AudioClip[] list)
    {
        if (list.Length > 0)
        {
            var sound = Random.Range(0, list.Length);
            audioPlayer.clip = list[sound];
        }
        if (audioPlayer != null && audioPlayer.clip != null) audioPlayer.Play();
    }

    public void PlaySound(AudioClip clip)
    {
        audioPlayer.clip = clip;
        if (audioPlayer != null && audioPlayer.clip != null) audioPlayer.Play();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("IsMoving", IsMoving());
        if (IsMoving())
        {
            stepTimer -= Time.deltaTime;
            if (stepTimer < 0)
            {
                PlayRandomSound(stepSounds);
                stepTimer = 0.6f;
            }
        }

        HandleUnitSelect();
        HandleMoveAction();
        HandleThrowableAction();

        AccurateTurnSpeed();
    }

    void HandleUnitSelect()
    {
        // не выбираем юнита если в руках что-то есть для броска, над бросить сначала
        if (IsThrowableSelected()) return;

        if (Input.GetButtonDown("SelectUnit"))
        {
            int layerMask = 1 << 12;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
            {
                if (hitInfo.collider == selectionCollider || hitInfo.collider == myCollider)
                {
                    SetSelected(true);
                    return;
                }
            }
        }
    }

    void HandleMoveAction()
    {
        // не отдаём приказ двигаться, если в руках есть что-то, снала отменим.
        if (IsThrowableSelected()) return;

        if (selected && Input.GetButtonDown("MoveOrAttack"))
        {
            int layerMask = 1 << 13;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
            {
                if (hitInfo.collider.tag == "WalkableTerrain")
                {
                    agent.SetDestination(hitInfo.point);
                    SetSelected(false);
                }
            }

        }
    }

    void HandleThrowableAction()
    {
        // Выбирать только если выбран юнит.
        if (!selected) return;

        if (CheckSelected(KeyCode.Q, 0) || CheckSelected(KeyCode.W, 1) || CheckSelected(KeyCode.E, 2))
        {
            return;
        }

        if (!IsThrowableSelected()) return;

        // Отменять на пкм
        if (Input.GetButtonDown("MoveOrAttack"))
        {
            UnselectThrowable();
            return;
        }

        // Бросать на лкм
        if (Input.GetButtonDown("SelectUnit"))
        {
            Vector3 start = transform.position;
            start.y = 4;

            EquipmentScript throwable = unitState.Equipment[throwableSelected];

            GameObject newThrowable = Instantiate(throwable.gameObject, start, transform.rotation);
            newThrowable.transform.LookAt(aimer.transform.position);

            ThrowableItem ti = newThrowable.GetComponent<ThrowableItem>();

            if (ti == null)
            {
                Debug.LogWarning("!! Selected Item is not ThrowableItem, but: " + newThrowable);
            }

            unitState.Equipment[throwableSelected] = null;

            UnselectThrowable();
            SetSelected(false);
        }
    }

    bool CheckSelected(KeyCode key, int idx)
    {
        if (Input.GetKeyDown(key) && unitState.Equipment.Count > idx && unitState.Equipment[idx] != null)
        {
            if (aimer != null) aimer.Show();
            throwableSelected = idx;
            return true;
        }
        return false;
    }

    bool IsThrowableSelected()
    {
        return throwableSelected > -1;
    }

    void UnselectThrowable()
    {
        throwableSelected = -1;
        if (aimer != null) aimer.Hide();
    }

    void AccurateTurnSpeed()
    {
        float speedMultiplyer = 1.0f - 0.9f * Vector3.Angle(transform.forward, agent.steeringTarget - transform.position) / 180.0f;
        agent.speed = baseSpeed * speedMultiplyer;
    }

    public void EarnedHP(int v)
    {
        PlaySound(hpSound);
        unitState.EarnedHP(v);
        healthBar.Show(1f, 360f * unitState.Health / 100f);
    }

    public bool IsDead()
    {
        return unitState.IsDead();
    }

    public bool IsMoving()
    {
        return agent.velocity.magnitude > 0.5;
    }

    public void Hit(int count)
    {
        if (unitState.IsDead()) return;
        PlayRandomSound(hurtSounds);
        unitState.Health -= count;

        if (unitState.Health < 0) unitState.Health = 0;

        healthBar.Show(1f, 360f * unitState.Health / 100f);

        //todo: death?


        if (unitState.IsDead())
        {
            UnselectThrowable();
            OnDie?.Invoke(this);
            Destroy(gameObject);
        }
    }


    override public string ToString()
    {
        return unitState.Name + "(hp=" + unitState.Health + "), has " + unitState.Equipment.Count + " items";
    }

}
