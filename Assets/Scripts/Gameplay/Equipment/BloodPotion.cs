﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodPotion : MonoBehaviour
{
    public int bloodAmount = 1;

    bool collected = false;

    ControllableUnitScript unit;

    private void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (collected) return;

        unit = other.gameObject.GetComponent<ControllableUnitScript>();

        if (unit != null)
        {
            collected = true;

            unit.EarnedHP(bloodAmount);
            Destroy(gameObject, 0.2f);
        }
    }

    private void Update()
    {
        transform.Rotate(0, 60 * Time.deltaTime, 0);

        if (unit != null)
        {
            transform.position = Vector3.Lerp(transform.position, unit.transform.position, 10 * Time.deltaTime);
        }
    }
}
