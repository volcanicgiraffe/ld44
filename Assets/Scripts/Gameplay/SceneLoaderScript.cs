﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderScript : MonoBehaviour
{
    private Scene sceneToBeActive;
    private Scene soundScene;
    private bool needToActivateScene = true;
    public bool MuteMusic = false;

    private float timerMusic = 0;
    private bool reenableMusic = false;

    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.sceneCount == 1)
        {
            SceneManager.LoadScene("Scenes/Gameplay/BasicScene", LoadSceneMode.Additive);
            SceneManager.LoadScene("Scenes/Gameplay/Terrain1/Terrain1", LoadSceneMode.Additive);
            sceneToBeActive = SceneManager.GetSceneAt(1);
            soundScene = SceneManager.GetSceneAt(2);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (needToActivateScene && sceneToBeActive.isLoaded)
        {
            SceneManager.SetActiveScene(sceneToBeActive);
            needToActivateScene = false;

        }
        if (MuteMusic && soundScene.isLoaded)
        {
            soundScene.GetRootGameObjects()[0].GetComponent<AudioSource>().Stop();
            soundScene.GetRootGameObjects()[0].GetComponent<AudioSource>().PlayDelayed(12);
            MuteMusic = false;
        }
    }
}

