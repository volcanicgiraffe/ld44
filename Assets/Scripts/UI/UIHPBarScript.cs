﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHPBarScript : MonoBehaviour
{

    public float HealthFromMax = 0.5f;
    private float oldHealth = -1111f;
    private RawImage blood;
    // Start is called before the first frame update
    void Start()
    {
        blood = transform.Find("Blood").GetComponent<RawImage>();
    }

    // Update is called once per frame
    void Update()
    {
        if (oldHealth != HealthFromMax)
        {
            oldHealth = HealthFromMax;
            blood.transform.localScale = new Vector3(1, HealthFromMax, 1);
        }
    }
}
