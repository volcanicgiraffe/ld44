﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDamage : MonoBehaviour
{
    public int damage = 1;
    public float destroyTime = 0f;

    bool canHit = true;

    private void OnTriggerEnter(Collider other)
    {
        // гард от двойнго урона
        if (!canHit) return;
        canHit = false;

        SimpleEnemy enemy = other.gameObject.GetComponent<SimpleEnemy>();

        if (enemy != null)
        {
            enemy.OnHit(this);
        }

        Destroy(gameObject, destroyTime);
    }
}