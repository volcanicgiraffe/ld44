﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUIScript : MonoBehaviour
{
    public Image SelectedUnit;
    public UIEquipmentScript SelectedEquipment;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowCompleted()
    {
        GetComponent<Animator>().SetTrigger("Complete");
    }

    public void ShowFailed()
    {
        GetComponent<Animator>().SetTrigger("Failed");
    }

    public void UpdateSelected(UnitState unitState)
    {

        if (unitState == null)
        {
            // тупой юнити почему-то именно тут не скрывает по enabled=false
            // я вообще молчу о том, что какого хера то проперти называется enabled, где visible
            SelectedUnit.transform.localScale = new Vector3(0, 0, 0);
            SelectedEquipment.transform.localScale = new Vector3(0, 0, 0);
            return;
        }
        SelectedUnit.transform.localScale = new Vector3(1, 1, 1);
        SelectedEquipment.transform.localScale = new Vector3(1, 1, 1);
        SelectedUnit.transform.Find("BloodContainer").GetComponent<UIHPBarScript>().HealthFromMax = unitState.HealthFromMax();
        SelectedUnit.transform.Find("Name").GetComponent<Text>().text = unitState.Name.ToUpper();
        SelectedUnit.transform.Find("HP").GetComponent<Text>().text = unitState.Health + "";
        SelectedEquipment.SetUnitState(unitState);
    }

}
